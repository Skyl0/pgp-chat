/**
 * TODO TO DIST FOLDER
 * Variables should be hidden
 */

var config = require('config.json')('./config/config.json');

/**
 * Requirements
 */
const express = require('express');
const path = require('path');
const app = express();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const io = require('socket.io')(server);
const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');
const CryptoJS = require("crypto-js");

/**
 * JSON Webtoken
 */

const jwt = require('jsonwebtoken');
const cpriv = fs.readFileSync(config.ssh.priv);
const cpub = fs.readFileSync(config.ssh.pub);

/**
 * Bodyparser
 */

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/**
 * Connect to DB
 */
MongoClient.connect(config.url_mongo, function (err, db) {
  if (!err) {
    console.log("MongoDB Connect successful");
  }

  /**
   * Socket IO
   */

  io.sockets.on('connection', function (socket) {

    console.log('user connected');
    /**
     * On Disconnect
     */
    socket.on('disconnect', function () {

    /*  console.log(socket);
      sysMsg = {
        sender: "SYSTEM",
        msg: "User Disconnect -> ",
        mention: socket.username,
        roomname: socket.room,
        timestamp: Date.now()
      };

      io.sockets.to(socket.room).emit("message", sysMsg);
      db.collection("chat-room").update({roomname: socket.room}, {$pull: {users: {alias: socket.username}}}, (err, data) => {
        if (err)throw err;
        //console.log(data);
      });
      socket.leave(socket.room); */

      console.log('user disconnected');
    });

    var usernames = {};
    var numUsers = 0;
    // when the client emits 'add user', this listens and executes
    socket.on('subscribe', function (data) {
      // we store the username in the socket session for this client

      console.log("Useralias " + data.useralias);

      db.collection("user").findOne({alias: data.useralias}, {
        alias: 1,
        publickey: 1,
        verified: 1
      }, function (err, user) {

        if (err) throw err;

        if (socket.room) socket.leave(socket.room);
        console.log("JOINED ROOM '" + data.roomname + "'");
        socket.join(data.roomname);

        db.collection("chat-room").update({roomname: data.roomname}, {$addToSet: {users: user}}, (err, roomObj) => {
          if (err) throw err;

          // console.log(roomObj.user);

        });

        sysMsg = {
          sender: "SYSTEM",
          msg: user.alias + " joined.",
          userObj: user,
          roomname: data.roomname,
          timestamp: Date.now()
        };

        console.log(sysMsg);

        io.sockets.to(data.roomname).emit("message", sysMsg);
      });

      //console.log(socket.rooms);

      // add the client's username to the global list
      usernames[data.useralias] = data.useralias;
      ++numUsers;
      addedUser = true;
      socket.emit('login', {
        numUsers: numUsers
      });
    });

    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });

    socket.on('keys added', function (data) {
      msg = {
        sender: "SYSTEM",
        msg: data.username + " is ready.",
        timestamp: Date.now()
      }
      console.log("KEY ADDED for " + data.username);//'" + msg + "'");
      io.sockets.to(data.roomname).emit("message", msg);
    });

    socket.on('msg', function (msg) {
      console.log("MSG TO ROOM '" + msg.roomname + "'");
      io.sockets.to(msg.roomname).emit("message", msg);
    });

    socket.on('unsubscribe', function (data) {
      console.log("Unsubscribe");
      sysMsg = {
        sender: "SYSTEM",
        msg: data.username + " left the channel",
        timestamp: Date.now()
      };

      io.sockets.to(data.room).emit("message", sysMsg);
      db.collection("chat-room").update({roomname: data.room}, {$pull: {users: {alias: data.username}}}, (err, data) => {
        if (err)throw err;
        console.log(data);
      });
      socket.leave(data.room);

    });

  });

  /**
   * Static Folders
   */

  app.use(express.static(path.join(__dirname, "dist")));
  app.use(express.static(path.join(__dirname, "public_html")));

  /**
   * API
   */

  var apiRoutes = express.Router();

  apiRoutes.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, cpub, function (err, decoded) {
        if (err) {
          return res.json({success: false, message: 'Failed to authenticate token.'});
        } else {
          // if everything is good, save to request for use in other routes
          if (req.baseUrl == "/api/chatroom") {
            // if its chatroom , generate a hash for the room name
            var temp = Date.now() + decoded.identifier + Math.random(2048);

            req.roomname = CryptoJS.AES.encrypt(temp, cpub.toString());
          }
          req.decoded = decoded;
          next();
        }
      });

    } else {

      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });

    }
  });

  app.use('/api/user', apiRoutes);
  app.use('/api/pgp', apiRoutes);
  app.use('/api/chatroom', apiRoutes);
  app.use('/api/user/*', apiRoutes);
  app.use('/api/pgp/*', apiRoutes);
  app.use('/api/chatroom/*', apiRoutes);
  require('./api/main')(app, db, io, jwt, cpriv);

  /**
   * Catch ALL <> Angular
   */

  app.get('**', function (req, res) {
    res.sendFile(__dirname + '/dist/index.html');
  });

  /**
   * Deploy
   */

  server.listen(config.port, function () {
    console.log("Listening on port " + config.port);
  });

});
