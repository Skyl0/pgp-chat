Yo Gude!

Kurzer Einstieg für alle Noobs um diese Projekt lokal zu betreiben:
(ME : Windows 10 Host)

1. ) Installiere Node.js (ME: 6.9.2)
2. ) Installiere ein Update für NPM (falls nötig) (Version ME: 3.10.9)
3. ) Installiere Angular CLI mit `npm install -g @angular/cli`  ! Wichtig -g für global
4. ) Installiere MongoDB (noch nicht verwendet aber sinnvoll)] 
5. ) Build die Angular App mit `ng build`
6. ) Installiere NodeMon mit `npm install -g nodemon`
7. ) Starte Express Server mit `nodemon server_main.js`
8. ) [localhost:8000](http://localhost:8000) aufrufen!



TBC
