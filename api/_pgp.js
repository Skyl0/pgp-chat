const openpgp = require('openpgp');

module.exports = function (app, db, socket, jwt, privkey) {

  const entrypoint = "/api/pgp";
  const User = db.collection("user");
//  const Answers = db.collection("answers");

  app.post(entrypoint + "/addpubkey", function (req, res) {
// public key zum eigenen Account hinzufügen
    console.log(req.body);
    if (req.body) {
    // User.findOne({publickey: req.body.publickey}); TODO look if user already registered a key
      User.update({alias: req.decoded.identifier}, {$set: {publickey: req.body.publickey}});
      res.json({success: true});
    } else {
      res.json({success: false});
    }
  });

  app.get(entrypoint + "/verify", (req, res) => {

    User.findOne({alias: req.decoded.identifier}, (err, usr) => {
      if (err ) throw err;

      var options, encrypted;

     // console.log(usr);


      options = {
        data: stringGen(64),                           // input as String (or Uint8Array)
        publicKeys: openpgp.key.readArmored(usr.publickey).keys,  // for encryption
      };

      // data to user for later
      User.update( {alias: req.decoded.identifier}, { $set : { decodethis : options.data}} );

      openpgp.encrypt(options).then(function (ciphertext) {
        encrypted = ciphertext.data; // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
        res.json({ "decodethis" : encrypted });
      });


    });

  });

  app.post(entrypoint + "/verify", (req, res) => {

   // console.log(req.body.answer);

    User.update({alias: req.decoded.identifier, decodethis : req.body.answer}, { $set : { verified : true}}, (err,user) => {
      if (err) throw err;


     if(user.result.nModified) {
       res.json({success: true});
     } else {
       res.json({success: false});
     }

    });

  });

  function stringGen(len) {
    var text = "";

    var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
      text += charset.charAt(Math.floor(Math.random() * charset.length));

    return text;
  }

};
