// _chatroom.js

module.exports = function (app, db, socket, jwt, privkey) {

  const entrypoint = "/api/chatroom";
  const ChatRoom = db.collection("chat-room");
  const User = db.collection("user");

  app.post(entrypoint, function (req, res) {
    // create new chatroom for user, first generate an unique hash

    var hash = req.roomname.toString();
    hash = hash.replace(/[^a-zA-Z0-9]/g, '');
    hash = hash.substr(hash.length - 17, 16);

    User.findOne({"alias": req.decoded.identifier}, {"alias": 1, "publickey": 1, "_id": 0}, function (err, user) {
      if (err) throw err;

      console.log(user);

      if (user) {
        console.log(user.alias + " created a Room.");
        var newRoom = {
          "users": [],
          "roomname": hash
        };

        //newRoom.users.push(user); TODO maybe remove or put it back in

        console.log(newRoom);

        ChatRoom.insertOne(newRoom, function (err, room) {
          if (err) throw err;

          res.json(newRoom);
        });
      }
    });

  });

  app.get(entrypoint + "/:room", (req, res) => {
    ChatRoom.findOne({roomname: req.params.room}, (err, room) => {
      if (err) throw err;
      console.log(room);
      res.send(room);
    });
  })

};
