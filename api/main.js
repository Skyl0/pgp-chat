// main.js
const userRoutes = require('./_user');
const chatRoomRoutes = require('./_chatroom');
const pgpRoutes = require('./_pgp');

module.exports = function (app, db, socket, jwt, privkey) {

  userRoutes(app, db, socket, jwt, privkey);
  chatRoomRoutes(app, db, socket, jwt, privkey);
  pgpRoutes(app, db, socket, jwt, privkey);
  // add more entrypoints
}
