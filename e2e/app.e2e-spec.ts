import { PgpChatPage } from './app.po';

describe('pgp-chat App', () => {
  let page: PgpChatPage;

  beforeEach(() => {
    page = new PgpChatPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
