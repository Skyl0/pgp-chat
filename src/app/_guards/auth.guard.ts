﻿import {Injectable} from "@angular/core";
import {CanActivate, Router} from "@angular/router";
import {AuthenticationService} from "../_services/authentication.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthenticationService) {
  }

  canActivate() {
    if (this.auth.loggedIn()) {
      // logged in so return true
      console.log("is logged in");
      return true;
    }

    // not logged in so redirect to login page
    //this.router.navigate(['/']);
    console.log("is not logged in");
    return false;
  }
}
