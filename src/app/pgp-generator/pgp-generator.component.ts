import {Component, OnInit} from "@angular/core";
import {PgpService} from "../_services/pgp.service";
import {UserService} from "../_services/user.service";
//import { saveAs } from 'file-saver';
import * as saveAs from 'file-saver';

//let fs = require('../_helpers/FileSaver.js');

@Component({
  selector: 'app-pgp-generator',
  templateUrl: './pgp-generator.component.html',
  styleUrls: ['./pgp-generator.component.css']
})
export class PgpGeneratorComponent implements OnInit {

  generateObj: any = {};
  recObj: any = {
    priv: "wait...",
    pub: "wait..."
  };
  loaded: boolean;

  downloadPriv : string;
  downloadPub : string;
  prefix = "data:text/plain;charset=utf-8,";

  constructor(private pgp: PgpService, public usr: UserService) {

  }

  ngOnInit() {
    this.loaded = false;
  }

  getKeys() {
    let tempObj: any = {
      priv: "wait...",
      pub: "wait..."
    };

    this.generateObj.user = this.usr.getAlias();
    this.pgp.generateKeys(this.generateObj).then(function (key) {
      let pri = key.privateKeyArmored;
      let pub = key.publicKeyArmored;
      //   console.log(pri + " " + pub);
      tempObj.priv = pri;
      tempObj.pub = pub;
      //   this.pgp.setPrivate(pri);
      //   this.pgp.setPublic(pub);
      // this.loaded = true;
      console.log(tempObj);

    }).finally(() => {
      this.recObj = tempObj;
      this.loaded = true;
    //  this.downloadPriv = this.prefix + this.recObj.priv;
   //   this.downloadPub = this.prefix + this.recObj.pub;
    });

    // this.generateObj = {};
  }


  createDownload(data: any) {
    let file = new Blob([data], {type: "text/plain;charset=utf-8"});
 //   window.document(window.URL.createObjectURL(data));
    //window.open(window.URL.createObjectURL(file));
    saveAs(file, "rename_key.txt");
  }
}
