import {Injectable} from "@angular/core";

@Injectable()
export class ConversionService {

  constructor() {
  }


  Uint8ToString(array: Uint8Array): string {
    let result: string = "";
    for (var i = 0; i < array.length; i++) {
      result += String.fromCharCode(array[i]);
    }

    return result;
  }


  StringToUint8(string: string): Uint8Array {

    let result : Uint8Array = new Uint8Array(string.length);
    for (var i = 0; i < string.length; i++) {
      result[i] = string.charCodeAt(i);
    }

    return result;
  }
}
