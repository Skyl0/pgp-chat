import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timestamp'
})
export class UnixTsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let date = new Date(value);

    let str = "";
    let hours = date.getHours() + "";
  //  if (hours.length<2) hours += "0";
    str += hours + ":";
    let minutes = date.getMinutes() + "";
    if (minutes.length<2) minutes += "0";
    str += minutes + ":";
    let seconds = date.getSeconds() + "";
    if (seconds.length<2) seconds += "0";
    str += seconds;

    return str;
  }

}
