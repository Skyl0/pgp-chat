import {Component, OnInit} from "@angular/core";
import {ChatService} from "../_services/chat.service";

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit {

  roomString: string = "";
  roomList: any = {
    rooms: [
      {name: "Pub1", user: 0},
      {name: "Pub2", user: 0}
    ]
  };

  constructor(private chatServ: ChatService) {
  }

  ngOnInit() {
  }

  createRoom() {
    this.chatServ.createChatroom().subscribe(
      data => {
        console.log(data);
        this.roomString = data.roomname;
      },
      error => {
        console.log(error);
      }
    );
  }

}
