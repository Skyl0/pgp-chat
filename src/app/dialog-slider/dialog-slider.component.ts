import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog-slider',
  templateUrl: './dialog-slider.component.html',
  styleUrls: ['./dialog-slider.component.css']
})
export class DialogSliderComponent implements OnInit {

  step = 1;

  constructor() { }

  ngOnInit() {
  }

}
