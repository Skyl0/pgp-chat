import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class VerifyService {

  constructor(private http: Http) {


  }


  postAnswer(answer : any) {

    let headers = new Headers({'x-access-token': localStorage.getItem("token")});
    let options = new RequestOptions({headers: headers});

    return this.http.post("/api/pgp/verify", answer, options)
      .map((response: Response) => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  postKey(pgppub: any) {

    let headers = new Headers({'x-access-token': localStorage.getItem("token")});
    let options = new RequestOptions({headers: headers});

    return this.http.post('/api/pgp/addpubkey', pgppub, options)
      .map((response: Response) => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  getPgpChallenge() {

    let headers = new Headers({'x-access-token': localStorage.getItem("token")});
    let options = new RequestOptions({headers: headers});

    // get users from api
    return this.http.get('/api/pgp/verify', options)
      .map((response: Response) => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
