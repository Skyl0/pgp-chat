﻿import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import {tokenNotExpired} from "angular2-jwt";
import {UserService} from "./user.service";
import {User} from "../_models/user";

@Injectable()
export class AuthenticationService {
  public token: string;

  constructor(private http: Http, private usr: UserService) {
    // set token if saved in local storage
    this.token = localStorage.getItem('token');
  }

  loggedIn() {
    return tokenNotExpired();
  }

  login(loginJson: any): Observable<boolean> {
    // console.log(loginJson);
    return this.http.post('/api/login', loginJson)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        console.log(response.json());
        let token = response.json() && response.json().token;
        if (token) {
          // set token property
          this.token = token;

          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', this.token);
          this.usr.updateUser();

          // return true to indicate successful login
          return true;
        } else {
          // return false to indicate failed login
          return false;
        }

      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    this.usr.current = new User("", "", "");
    localStorage.removeItem('token');
  }
}
