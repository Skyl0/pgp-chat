import {Injectable} from "@angular/core";
import * as io from "socket.io-client";
import {Observable} from "rxjs/Observable";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {ChatRoom} from "../_models/chat-room";
import {Message} from "../_models/message";
import {AuthenticationService} from "./authentication.service";
import {UserService} from "./user.service";
import {JoinMessage} from "../_models/join-msg";
import {User} from "app/_models/user";

@Injectable()
export class ChatService {

  private socket;
  private currentUsers;

  constructor(private http: Http, private auth: AuthenticationService, private usr: UserService) {
    this.socket = io();
 //   this.updateUsers();
  }

  sendServiceMsg(sysmsg : string, obj: any) {
    console.log(obj);
    this.socket.emit(sysmsg,obj);
  }

  sendMessage(message: Message) {
    console.log(message);
    this.socket.emit('msg', message);
  }

  getMessages(room: string): Observable<Message> | any {

    let observable = new Observable<Message>(observer => {

      this.usr.updateUser().then(data => {
        var userData = new JoinMessage(data.alias, room);
        console.log(userData);
        this.socket.emit('subscribe', userData);
      //  this.socket.emit('get channel', {room: room});
      });

      this.socket.on('message', (data) => {
        observer.next(data);
      });
      return () => {

        this.socket.disconnect();
      };
    });
    return observable;
  }

  updateUsers() {
    this.getUsersInChannel().subscribe(
      data => {
        console.log(data);
        this.currentUsers = data;
      },
      error => {

      }
    )
  }

  getUsersInChannel(): any {

    let observable = new Observable<Message>(observer => {

      this.socket.on('channel', (data) => {
        observer.next(data);
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  leaveRoom(room: string) {
    this.socket.emit('unsubscribe', room);
  }

  createChatroom(): Observable<ChatRoom> {
    let headers = new Headers({'x-access-token': this.auth.token});
    let options = new RequestOptions({headers: headers});

    return this.http.post("/api/chatroom", {}, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getChatroom(room: string): Observable<ChatRoom> {
    let headers = new Headers({'x-access-token': this.auth.token});
    let options = new RequestOptions({headers: headers});

    return this.http.get("/api/chatroom/" + room, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


}
