import {Injectable} from "@angular/core";

declare var openpgp: any;

@Injectable()
export class PgpService {


  private priv_key: string = "";
  private pub_key: string = "";
  private passphrase: string = "";

  constructor() {
  }


  generateKeys(req : any): any {

    var options = {
      userIds: [{name: req.name, email: req.email}], // multiple user IDs
      numBits: 4096,                                            // RSA key size
      passphrase: req.pass         // protects the private key
    };

    return openpgp.generateKey(options);

  }


  encrypt(msg: string, key_partner: string): any {


    if (!this.keysLoaded()) {
      console.log("ERROR: Keys are missing");
    }

    let privKeyObj = openpgp.key.readArmored(this.priv_key).keys[0];
    privKeyObj.decrypt(this.passphrase);

    let options = {
      data: msg,                             // input as String (or Uint8Array)
      publicKeys: openpgp.key.readArmored(key_partner).keys,  // for encryption
      privateKeys: privKeyObj // for signing (optional)
    };

    return openpgp.encrypt(options);
  }

  decrypt(msg: string): any {
    if (!this.keysLoaded()) {
      console.log("ERROR: Keys are missing");
    }

    let privKeyObj = openpgp.key.readArmored(this.priv_key).keys[0];
    privKeyObj.decrypt(this.passphrase);


    let options = {
      message: openpgp.message.readArmored(msg),       // parse armored message
      publicKeys: openpgp.key.readArmored(this.pub_key).keys,    // for verification (optional)
      privateKey: privKeyObj  // for decryption
    };

    return openpgp.decrypt(options);
  }

  setPassphrase(value: string) {
    this.passphrase = value;
  }

  keysLoaded(): boolean {
    return (this.priv_key != "" && this.pub_key != "");
  }

  setPrivate(key: any): void {
    this.priv_key = key;
  }

  setPublic(key: any): void {
    this.pub_key = key;
  }

  getPublic(): string {
    return this.pub_key;
  }
}
