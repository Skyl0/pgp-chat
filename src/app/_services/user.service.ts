﻿import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {User} from "../_models/user";
import {Observable} from "rxjs/Observable";
import {PgpService} from "./pgp.service";

@Injectable()
export class UserService {

  public current: User = null;

  constructor(private http: Http, private pgp: PgpService) {
    this.dropUserdata();
    this.updateUser();
  }

  dropUserdata(): void {
    this.current = {
      verified: false,
      username: "",
      alias: "",
      publickey: ""
    };
  }

  userLoaded() : boolean {
    return this.current.username != "";
  }

  updateUser(): any {
    return new Promise((resolve, reject) => {
      if (this.current.username != "") resolve(this.current);
      else {
        this.getUser().subscribe(
          data => {
            //   console.log(data);
            this.current = data;
            this.pgp.setPublic(this.current.publickey);
            resolve(data);
          },
          error => {
            console.log(error);
            reject();
          }
        );
      }
    });

  }

  getAlias() {
    return this.current.alias;
  }

  getCurrentUser() {
    return this.current;
  }

  private getUser(): Observable<User> {
    // add authorization header with jwt token
    //  console.log("Token: " + localStorage.getItem("token"));
    let headers = new Headers({'x-access-token': localStorage.getItem("token")});
    let options = new RequestOptions({headers: headers});

    // get users from api
    return this.http.get('/api/user', options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  registerUser(user: any): any {
    return this.http.post('/api/register', user)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
