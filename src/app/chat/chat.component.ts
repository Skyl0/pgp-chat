import {Component, OnDestroy, OnInit} from "@angular/core";
import {ChatService} from "../_services/chat.service";
import {ChatRoom} from "../_models/chat-room";
import {ActivatedRoute, Router} from "@angular/router";
import {Message} from "../_models/message";
import {UserService} from "../_services/user.service";
import {ConversionService} from "../_helpers/conversion.service";
import {PgpService} from "../_services/pgp.service";
import {User} from "../_models/user";
import {ScrollGlue} from 'angular2-scroll-glue';

import * as $ from 'jquery';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  messages: Message[] = [];
  connection;
  message: string = "";
  room: ChatRoom;
  aroute: string = "";
  key_partner: string;
  passphrase: string;
  loadedkey: boolean = false;
  setup: boolean = false;

  constructor(private chat: ChatService, private activatedRoute: ActivatedRoute, public usr: UserService, private conv: ConversionService, public pgp: PgpService, private router: Router) {}

  updateKeyAndPass() {
    if (this.loadedkey) {
      this.chat.sendServiceMsg("keys added", {username: this.usr.current.alias, roomname: this.room.roomname});
      this.pgp.setPassphrase(this.passphrase);
      this.setup = true;
    }
  }

  sendMessage(): void {
    if (this.usr.updateUser()) {

      var messageObj: Message;

      if (this.pgp.keysLoaded) {
        let myMessage: Message = new Message(this.usr.current.alias, this.message, this.room.roomname, Date.now());
        //this.messages.push(myMessage);

        this.pgp.encrypt(this.message, this.key_partner).then((cypher) => {
          this.messages.push(myMessage);
          messageObj = new Message(this.usr.current.alias, cypher.data, this.room.roomname, Date.now());
          console.log(messageObj.msg);
          this.message = "";
          this.chat.sendMessage(messageObj);

        });

      }
      else {
        messageObj = new Message(this.usr.current.alias, this.message, this.room.roomname, Date.now());
        console.log(messageObj.msg);
        this.message = "";
        this.chat.sendMessage(messageObj);
      }
    }
  }

  changeListener($event): void {

    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var fr: FileReader = new FileReader();
    // console.log(this.sha_hash);
    fr.readAsArrayBuffer(file);

    fr.onload = (data) => {
      this.pgp.setPrivate(this.conv.Uint8ToString(new Uint8Array(fr.result)));
      this.loadedkey = true;

      this.pgp.setPublic(this.usr.current.publickey);
      this.pgp.setPassphrase(this.passphrase);

    }

  }

  private getKeyPartner() {

    let users: User[] = this.room.users;
    console.group("getKeyPartner");
    for (var i = 0; i < users.length; i++) {

      console.log(users[i]);

      if (users[i].alias != this.usr.getAlias()) {
        this.key_partner = users[i].publickey;
        console.log(this.key_partner);
        break;
      }

    }
    console.groupEnd();
  }

  leaveChannel() {
    this.chat.sendServiceMsg("unsubscribe", {username: this.usr.getAlias(), room: this.room.roomname});
    this.router.navigate(['chat-room']);
  }

  subscribeChannel() {


    if (this.usr.updateUser()) {
      this.aroute = this.activatedRoute.snapshot.params['room'];
      // console.log(this.aroute);
      this.connection = this.chat.getMessages(this.aroute).subscribe(message => {
        let firstRun = true;
        //if (message.sender == "SYSTEM" && message.userObj.alias != this.usr.getAlias()) this.key_partner = message.userObj.publickey;

        if (message.sender == "SYSTEM" || firstRun) {
          this.chat.getChatroom(this.aroute).subscribe(
            data => {
              this.room = data;
              this.getKeyPartner();
              firstRun = false;
            },
            error => {
              console.log(error);
            }
          );
        }

        //  console.log("pgp ? " + (message.msg.indexOf("-----BEGIN PGP MESSAGE-----") > -1));
        //  console.log("loaded keys ? " + this.pgp.keysLoaded());

        if (message.msg.indexOf("-----BEGIN PGP MESSAGE-----") > -1 && this.pgp.keysLoaded()) {
          let decrypted: Message = message;
          console.group("before decrypt");
          console.log(decrypted);
          console.groupEnd();

          if (decrypted.sender != this.usr.getAlias()) {
            this.pgp.decrypt(decrypted.msg).then((cypher) => {
              decrypted.msg = cypher.data;
              this.messages.push(decrypted);

              //  jq.css({'background-color': 'red'});
              jq.scrollTop(jq.prop("scrollHeight"));
            })
          }


        } else {
          this.messages.push(message);
          //  jq.css({'background-color': 'red'});

        }
        let jq = $('.message_out');
        jq.scrollTop(jq.prop("scrollHeight"));
      });

      this.chat.getChatroom(this.aroute).subscribe(
        data => {
          this.room = data;
          // console.log(data);
        },
        error => {
          // console.log(error);
        }
      );

    }

  }


  ngOnInit(): void {
    this.subscribeChannel();
  }

  ngOnDestroy(): void {
    this.chat.sendServiceMsg("unsubscribe",{username: this.usr.getAlias(), roomname: this.room.roomname});
    this.connection.unsubscribe();
  }

  handleEvent($event) {
    if ($event.which == 13 || $event.keyCode == 13) {
      this.sendMessage();
    }
  }


}
