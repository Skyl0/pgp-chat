import {Component, OnInit} from "@angular/core";
import {VerifyService} from "../_services/verify.service";
import {AuthenticationService} from "../_services/authentication.service";
import {UserService} from "../_services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {

  answer: any = {};
  tosolve: string = "";
  model: any = {};

  step: number;


  constructor(private veriServ: VerifyService, private auth: AuthenticationService, public usr: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.step = 0;
  }

  verifyMe() {

    this.veriServ.postAnswer(this.answer).subscribe(
      data => {
        console.log(data);
        this.router.navigate(["/"]);
      },
      error => {
        console.log(error.json);
      }
    );
  }


  updatePubKey() {

    this.usr.updateUser().then((valid) => {

      if (valid) {
        this.veriServ.postKey(this.model).subscribe(
          data => {
            if (data.success) {
              this.veriServ.getPgpChallenge().subscribe(
                data => {
                  console.log(data);
                  this.tosolve = data.decodethis;
                  this.step = 1;
                },
                error => {
                  console.log(error.json)
                }
              );
            }
          },
          error => console.log(error.json)
        );
      } else {
        console.log("No User loaded");
      }

    });
  }


}
