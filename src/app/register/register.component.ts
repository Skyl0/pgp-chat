import { Component, OnInit } from '@angular/core';
import {UserService} from "../_services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model : any = {};


  constructor(private usrServ: UserService, private router: Router) { }

  ngOnInit() {
  }

  doRegister() {
    this.usrServ.registerUser(this.model).subscribe(
      data => this.router.navigate(['/']),
      error => console.log(error)
  );
  }

}
