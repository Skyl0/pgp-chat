import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppComponent} from "./app.component";
import {ChatService} from "./_services/chat.service";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MdButtonModule, MdCheckboxModule} from "@angular/material";
import {LandingComponent} from "./landing/landing.component";
import {ChatComponent} from "./chat/chat.component";
import {ChatRoomComponent} from "./chat-room/chat-room.component";
import {RegisterComponent} from "./register/register.component";
import {LoginComponent} from "./login/login.component";
import {UserComponent} from "./user/user.component";
import {HttpModule} from "@angular/http";
import {RouterModule, Routes} from "@angular/router";
import {VerifyComponent} from "./verify/verify.component";
import {VerifyService} from "./_services/verify.service";
import {AuthenticationService} from "./_services/authentication.service";
import {UserService} from "./_services/user.service";
import {AuthGuard} from "./_guards/auth.guard";
import {ConversionService} from "app/_helpers/conversion.service";
import {PgpService} from "./_services/pgp.service";
import { UnixTsPipe } from './_helpers/unix-ts.pipe';
import { PgpGeneratorComponent } from './pgp-generator/pgp-generator.component';
import { shim } from 'promise.prototype.finally';
import { DialogSliderComponent } from './dialog-slider/dialog-slider.component';
shim();

var config = '../../config/config.js';

const appRoutes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'pgp-key-generator', component: PgpGeneratorComponent},
 // {path: 'login', component: LoginComponent},
  {path: 'chat/:room', component: ChatComponent, canActivate: [AuthGuard]},
  {path: 'create-room', component: ChatRoomComponent, canActivate: [AuthGuard]},
  {path: 'user/verify', component: VerifyComponent, canActivate: [AuthGuard]},
  {path: 'test', component: DialogSliderComponent},
  {path: '**', component: LandingComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    ChatComponent,
    ChatRoomComponent,
    RegisterComponent,
    LoginComponent,
    UserComponent,
    VerifyComponent,
    UnixTsPipe,
    PgpGeneratorComponent,
    DialogSliderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule, MdButtonModule, MdCheckboxModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [ChatService, AuthGuard, VerifyService, AuthenticationService, UserService, ConversionService, PgpService],
  bootstrap: [AppComponent]
})
export class AppModule {

  //console.log(config.url_site);
}
