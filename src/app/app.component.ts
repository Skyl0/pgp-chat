import {Component} from "@angular/core";
import {AuthenticationService} from "./_services/authentication.service";
import {UserService} from "./_services/user.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor (public auth : AuthenticationService, public userServ : UserService) {
    console.group("TokenCheck");
    console.log(localStorage.getItem("token"));
    console.groupEnd();
    if (localStorage.getItem("token") != null) this.userServ.updateUser();
  }
}


