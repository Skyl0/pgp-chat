import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../_services/authentication.service";
import {Router} from "@angular/router";
import {UserService} from "../_services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};

  constructor(public auth: AuthenticationService, private router: Router, public usr: UserService) {
  }

  ngOnInit() {
  }

  doLogin() {

    this.auth.login(this.model).subscribe(
      data => {
        //console.log(data);
        if (data) {
          this.router.navigate(['/']);
        } else {
          console.log("login not complete, no token rcv");
        }

      },
      error => console.log(error)
    );
  }

  doLogout() {
    this.auth.logout();
    this.router.navigate(['/']);
  }

  handleEvent($event) {
    if ($event.which == 13 || $event.keyCode == 13) {
      this.doLogin();
    }
  }

}
