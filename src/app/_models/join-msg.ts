export class JoinMessage {

  constructor(public useralias: string,
              public roomname: string) {
  }
}
