﻿export class User {
  public verified: boolean;
  public username;
  public alias;
  public publickey;

  constructor(
    username: string,// for login
    alias: string, // public to others
    publickey: string // PGP Public
  ){
    this.username = username;
    this.alias = alias;
    this.publickey = publickey;
    this.verified = false;
  }
}
