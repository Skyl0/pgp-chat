import {User} from "./user";
import {ChatRoom} from "./chat-room";

export class Message {
  public sender: string;
  public msg: string;
  public roomname: string;
  public timestamp: any;

  constructor(sender: string,
              msg: string,
              roomname: string,
              timestamp: any) {
    this.sender = sender;
    this.msg = msg;
    this.roomname = roomname;
    this.timestamp = timestamp;
  }

}
