import {User} from "./user";
export class ChatRoom {

  users : Array<User>;
  roomname : string; // must be something cryptic later
  socket : any ; // maybe to save socket info

  constructor () {
    this.users = new Array<User>();
  }
}
